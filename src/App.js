import React from 'react';
// import logo from './logo.svg';
// import Cookies from 'js-cookie'
// import axios from 'axios'
import { useVerifySession } from './utils/verifySession'
import './App.css';
import MainDrawer from './components/mainDrawer'
import { makeStyles } from '@material-ui/core/styles'
import Login from './components/user/login'
import CircularProgress from '@material-ui/core/CircularProgress'
import userContext from './userContext'

const useStyles = makeStyles((theme) => ({
  root: {
    witdth: '100%',
    height: '100%',
    marginTop: theme.spacing(10),
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
  item: {
    margin: 'auto'
  }
}))

function App() {
  const [verify, setVerify, name, setName, loading, user] = useVerifySession()

  const classes = useStyles()

  return (
    <div className="App">
      {
        loading
          ? (
            <div className={classes.root}>
              <CircularProgress className={classes.item} color="secondary" />
            </div>
          )
          : verify
            ? <userContext.Provider value={user}><MainDrawer name={name} /></userContext.Provider>
            : <Login setVerify={setVerify} setName={setName} />
      }
    </div>
  );
}

export default App;
