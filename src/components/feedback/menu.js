import React, { useEffect, useState } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { logout } from './../../utils'

export default function SimpleMenu({ target, setTarget }) {
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    setAnchorEl(target)
  }, [target])

  const handleClose = () => {
    setAnchorEl(null);
    setTarget(null)
  };

  return (
    <div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={logout}>Cerrar Sesión</MenuItem>
      </Menu>
    </div>
  );
}