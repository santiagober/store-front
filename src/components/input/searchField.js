import React, { useRef, useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
    marginTop: 0
  },
}));

export default function SearchField({ setSearch }) {
  const [value, setValue] = useState('')
  const input = useRef()
  const classes = useStyles()

  useEffect(() => {
    setTimeout(() => {
      if (value === input.current.value) {
        setSearch(value)
      }
    }, 800)
  }, [setSearch, value])

  const handleChange = (e) => {
    const val = e.target.value
    setValue(val)
  }

  const handleClickSearch = () => {
    const val = input.current.value
    console.log(val)
    setSearch(val)
  }

  return (
    <div className={classes.margin}>
      <Grid container spacing={1} alignItems="flex-end">
        <Grid item>
          <IconButton onClick={handleClickSearch} size={'small'} aria-label="search">
            <SearchIcon />
          </IconButton>
        </Grid>
        <Grid item>
          <TextField inputRef={input} onChange={handleChange} id="input-with-icon-grid" label="Buscar..." />
        </Grid>
      </Grid>
    </div>
  )
} 