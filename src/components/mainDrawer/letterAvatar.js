import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Menu from './../feedback/menu'

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: 'pointer',
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  }
}));


const letterUsername = (username) => username.substr(0, 2).toUpperCase()

export default function LetterAvatar({ username, openMenu }) {
  const [target, setTarget] = useState(null)
  const classes = useStyles();

  const handleClick = (e) => {
    console.log(username)
    setTarget(e.currentTarget)
  }

  return (
    <div className={classes.root}>
      {/* <Avatar>H</Avatar> */}
      {/* <Avatar className={classes.orange}>N</Avatar> */}
      <Avatar onClick={handleClick}>
        {
          letterUsername(username)
        }
      </Avatar>
      <Menu target={target} setTarget={setTarget} />
    </div>
  );
}