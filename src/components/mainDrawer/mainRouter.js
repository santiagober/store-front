import React from 'react'
import { Route } from 'react-router-dom'
import Products from './../ops/products'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
}));

export default function MainRouter() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Route exact path={'/ops/articulos'} component={Products} />
    </div>
  )
} 