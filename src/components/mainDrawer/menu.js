import React, { useState, useImperativeHandle, forwardRef } from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
// import { makeStyles, useTheme } from '@material-ui/core/styles';
import PageviewIcon from '@material-ui/icons/Pageview';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AssessmentIcon from '@material-ui/icons/Assessment';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';

const Ops = [
  { id: '1.1', text: 'Ventas', path: '/ops/ventas' },
  { id: '1.2', text: 'Devoluciones', path: 'ops/devoluciones' },
  { id: '1.3', text: 'Compras', path: '/ops/notcred' },
  { id: '1.4', text: 'Pedidos', path: '/ops/pedidos' },
  { id: '1.5', text: 'Cortes de caja', path: '/ops/cortcaj' },
  { id: '1.6', text: 'Artículos (catálogo)', path: '/ops/articulos' },
  { id: '1.7', text: 'Clientes', path: '/ops/clientes' },
  { id: '1.8', text: 'Proveedores', path: '/ops/proveedores' },
  { id: '1.9', text: 'Inventario inicial', path: '/ops/inveini' },
  { id: '1.10', text: 'Ajuste de inventario', path: '/ops/ajusinve' },
  { id: '1.11', text: 'Lotes / Series', path: '/ops/lotser' },
  // 'Ventas',
  // 'Devoluciones',
  // 'Compras',
  // 'Notas de crédito',
  // 'Pedidos',
  // 'Cortes de caja',
  // {
  //   id: '1',
  //   text: 'Artículos (catálogo)'
  // },
  // 'Artículos (catálogo)',
  // 'Clientes',
  // 'Proveedores',
  // 'Inventario Inicial',
  // 'Ajuste de Inventario',
  // 'Lotes / Series'
]

const menuQuery = [
  'Ventas',
  'Devoluciones',
  'Compras',
  'Notas de crédito',
  'Clientes',
  'Proveedores',
  'Movimientos caja',
  'Cortes caja',
  'Pedidos',
  'Inventario inicial',
  'Ajustes inventario',
  'Lotes',
  'Articulos'
]

const Menu = forwardRef(({ history }, ref) => {
  const [openOps, setOpenOps] = useState(false)
  const [openQuery, setOpenQuery] = useState(false)

  useImperativeHandle(ref, () => ({
    dispatchCloseMenu: () => {
      setOpenOps(false)
      setOpenQuery(false)
    }
  }))

  const menuOps = Ops.map(op => ({
    ...op,
    [`handle${op.id}`]: function () {
      history.push(op.path)
    }
  }))

  const handleClickQuery = () => {
    setOpenQuery(!openQuery);
  }

  const handleClickOps = () => {
    setOpenOps(!openOps);
  }

  // const handleDefault = () => {
  //   history.push('/default')
  // }

  // const handleTest = () => {
  //   history.push('/ops/articulos')
  // }

  return (
    <List >
      <ListItem button key={'operation'} onClick={handleClickOps}>
        <ListItemIcon><InboxIcon /></ListItemIcon>
        <ListItemText primary={'Operaciones'} />
        {openOps ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={openOps} timeout="auto" unmountOnExit>
        <List dense component="div" disablePadding>
          {menuOps.map(item => (
            <ListItem button key={`${item.text}`} onClick={item[`handle${item.id}`]}>
              <ListItemText primary={`${item.text}`} />
            </ListItem>
          ))}
          {/* <ListItem button >
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText divider={true} primary="Ventas" />
              </ListItem> */}
        </List>
      </Collapse>
      <ListItem button key={'query'} onClick={handleClickQuery}>
        <ListItemIcon><PageviewIcon /></ListItemIcon>
        <ListItemText primary={'Consultas'} />
        {openQuery ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={openQuery} timeout="auto" unmountOnExit>
        <List dense component="div" disablePadding>
          {menuQuery.map(item => (
            <ListItem button key={`${item}`}>
              <ListItemText primary={`${item}`} />
            </ListItem>
          ))}
        </List>
      </Collapse>
      <ListItem button key={'process'}>
        <ListItemIcon><AutorenewIcon /></ListItemIcon>
        <ListItemText primary={'Procesos'} />
      </ListItem>
      <ListItem button key={'report'}>
        <ListItemIcon><AssignmentIcon /></ListItemIcon>
        <ListItemText primary={'Reportes'} />
      </ListItem>
      <ListItem button key={'statistics'}>
        <ListItemIcon><AssessmentIcon /></ListItemIcon>
        <ListItemText primary={'Estadísticas'} />
      </ListItem>
      <ListItem button key={'setting'}>
        <ListItemIcon><SettingsApplicationsIcon /></ListItemIcon>
        <ListItemText primary={'Configuración'} />
      </ListItem>
      {/* {['Operaciones', 'Consultas', 'Procesos', 'Reportes', 'Estadísticas', 'Configuración'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))} */}
    </List>
  )
})

export default Menu