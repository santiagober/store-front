import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Dialog,
  // MuiDialogTitle,
  // MuiDialogContent,
  // MuiDialogActions,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  IconButton,
  Typography,
  TextField,
  Grid,
  FormControlLabel,
  Checkbox,
  Divider
} from '@material-ui/core/'
// import Button from '@material-ui/core/Button';
// import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
// import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
// import Typography from '@material-ui/core/Typography';
// import TextField from '@material-ui/core/TextField'
// import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles((theme) => ({
  formControl: {
    // margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}))

const styles = (theme) => ({
  root: {
    margin: 0,
    // padding: theme.spacing(2),
  },
  rootGrid: {
    flexGrow: 1
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs({ article, setOpenParent }) {
  const [open, setOpen] = React.useState(true);

  const classes = useStyles()

  // const handleClickOpen = () => {
  //   setOpen(true);
  // }

  const onSendProduct = () => {

  }

  const handleClose = () => {
    setOpen(false)
    setOpenParent(false)
  }

  return (
    <div>
      {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open dialog
      </Button> */}
      <Dialog fullWidth maxWidth={'md'} onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        // aria-labelledby="customized-dialog-title" 
        open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {article ? 'Actualizar producto' : 'Registrar producto'}
          {console.log('article: ', article)}
        </DialogTitle>
        <DialogContent dividers>
          <div style={{ flexGrow: 1 }}>
            <Grid container spacing={1}>
              <Grid item xs={3}>
                <TextField
                  label="Clave"
                  id="outlined-size-small1"
                  variant="outlined"
                  size="small"
                  defaultValue={article.primaryKey}
                  // margin="dense"
                  fullWidth
                />
              </Grid>
              <Grid></Grid>
              <Grid item xs={3}>
                <TextField
                  label="Clave alterna"
                  id="outlined-size-small2"
                  variant="outlined"
                  size="small"
                  defaultValue={article.alternativeKey}
                  // margin="dense"
                  fullWidth
                />
              </Grid>
              <Grid item xs={6}>
                <Grid container justify={'flex-end'} spacing={0}>
                  <Grid item x={5} >
                    <FormControlLabel
                      control={
                        <Checkbox
                          // checked={state.checkedB}
                          // onChange={handleChange}
                          checked={article.service}
                          name="checkedB"
                          color="primary"
                        />
                      }
                      label="Servicio"
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Descripción"
                  id="outlined-size-small3"
                  variant="outlined"
                  size="small"
                  defaultValue={article.description}
                  // margin="dense"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Grid container spacing={3}>
                  <Grid item xs={2}>
                    <FormControl size="small" fullWidth className={classes.formControl}>
                      <InputLabel id="family-select-label">Familia</InputLabel>
                      <Select
                        labelId="family-select-label"
                        id="family-simple-select"
                        defaultValue={''}
                      // onChange={handleChange}
                      >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={2}>
                    <FormControl fullWidth size="small" className={classes.formControl}>
                      <InputLabel id="category-select-label">Categoria</InputLabel>
                      <Select
                        labelId="category-select-label"
                        id="category-simple-select"
                        defaultValue={''}
                      // value={''}
                      // onChange={handleChange}
                      >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={2}>
                    <FormControl fullWidth size="small" className={classes.formControl}>
                      <InputLabel id="subcategory-select-label">Sub Categoria</InputLabel>
                      <Select
                        labelId="subcategory-select-label"
                        id="subcategory-simple-select"
                        defaultValue={''}
                      // value={''}
                      // onChange={handleChange}
                      >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={2}>
                <FormControl fullWidth size="small" className={classes.formControl}>
                  <InputLabel id="PurchaseUnit-select-label">Unidad compra</InputLabel>
                  <Select
                    labelId="PurchaseUnit-select-label"
                    id="PurchaseUnit-simple-select"
                    defaultValue={''}
                  // value={''}
                  // onChange={handleChange}
                  >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={2}>
                <FormControl size="small" fullWidth className={classes.formControl}>
                  <InputLabel id="SaleUnit-select-label">Unidad venta</InputLabel>
                  <Select
                    labelId="SaleUnit-select-label"
                    id="SaleUnit-simple-select"
                    defaultValue={''}
                  // value={''}
                  // onChange={handleChange}
                  >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={2}>
                <TextField
                  label="Factor"
                  id="factor-size-small1"
                  variant="outlined"
                  size="small"
                  defaultValue={article.factor}
                  // margin="dense"
                  fullWidth
                />
              </Grid>
            </Grid>
          </div>
          <Divider variant={'fullWidth'} style={{ margin: '20px 0' }} />
          <div style={{ flexGrow: 1 }}>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                <TextField
                  label="Precio compra"
                  id="outlined-size-small1"
                  variant="outlined"
                  size="small"
                  // margin="dense"
                  fullWidth
                  defaultValue={article.purchasePrice}
                />
              </Grid>
              <Grid item x={2} >
                <FormControlLabel
                  control={
                    <Checkbox
                      // checked={state.checkedB}
                      // onChange={handleChange}
                      name="checkedNeto"
                      color="primary"
                      checked={article.neto}
                    />
                  }
                  label="Neto"
                />
              </Grid>
            </Grid>
          </div>
          {/* <TextField id="outlined-basic" label="Outlined" variant="outlined" /> */}

          {/* <Typography gutterBottom>
            Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis
            in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
          </Typography>
          <Typography gutterBottom>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis
            lacus vel augue laoreet rutrum faucibus dolor auctor.
          </Typography>
          <Typography gutterBottom>
            Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel
            scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus
            auctor fringilla.
          </Typography> */}
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Guardar
          </Button>
        </DialogActions>
      </Dialog>
    </div >
  );
}