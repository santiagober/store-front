import React, { useEffect, useState } from 'react'
import Paper from '@material-ui/core/Paper'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { green } from '@material-ui/core/colors'
import { fade, makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Axios from 'axios'
import Typography from '@material-ui/core/Typography'
import StorefrontIcon from '@material-ui/icons/Storefront'
// import EditIcon from '@material-ui/icons/Edit'
// import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import FileCopyIcon from '@material-ui/icons/FileCopy'
import AutorenewIcon from '@material-ui/icons/Autorenew'
import SearchField from './../input/searchField'
import SearchTable from './searchTable'
// import useRequest from './../../utils/request'
import LinearProgress from '@material-ui/core/LinearProgress'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  wrapOptions: {
    display: 'flex',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
  },
  body: {
    height: '100%'
  },
  iconAdd: {
    color: green[500]
  },
  iconEdit: {
    color: theme.palette.text.secondary
  },
  iconDelete: {
    color: theme.palette.error.main
  },
  iconClon: {
    color: theme.palette.primary.dark
  },
  iconResearch: {
    color: theme.palette.warning.main
  },
  iconAdjust: {
    color: theme.palette.text.primary
  },
  bodyProducts: {
    // height: 'auto',
    // marginTop: theme.spacing(1),
    // marginRight: theme.spacing(10),
  },
  header: {
    height: theme.spacing(10)
  },
  paper: {
    marginTop: theme.spacing(1.5),
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1),
    padding: theme.spacing(0.5),
    textAlign: 'center',
    overflow: 'hidden',
    width: theme.spacing(10),
    color: theme.palette.text.primary,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.15),
    // '&:hover': {
    //   backgroundColor: fade(theme.palette.common.white, 0.25),
    // },
    paddingRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function Products() {
  const [loading, setLoading] = useState(true)
  const [result, setResult] = useState([])
  const [search, setSearch] = useState('')
  const classes = useStyles()

  useEffect(() => {
    setLoading(true)
    Axios.get(`${process.env.REACT_APP_STORE_BACK}/product/list?search=${search}&limit=20&skip=0`)
      .then(({ data: { success, products } }) => {
        if (success)
          setResult(products)
        setLoading(false)
      })
      .catch((e) => {
        console.log(e)
      })
      .finally(() => setLoading(false))
  }, [search])

  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <div className={classes.wrapOptions}>
          <Paper className={classes.paper} elevation={2}>
            <AddCircleIcon className={clsx(classes.icon, classes.iconAdd)} />
            <Typography variant={'subtitle2'}>Agregar</Typography>
          </Paper>
          <Paper className={classes.paper} elevation={2}>
            <FileCopyIcon className={clsx(classes.icon, classes.iconClon)} />
            <Typography variant={'subtitle2'}>Clonar</Typography>
          </Paper>
          <Paper className={classes.paper} elevation={2}>
            <AutorenewIcon className={clsx(classes.icon, classes.iconEdit)} />
            <Typography variant={'subtitle2'}>Recargar</Typography>
          </Paper>
          <Paper className={classes.paper} elevation={2}>
            <StorefrontIcon className={clsx(classes.icon, classes.iconAdjust)} />
            <Typography variant={'subtitle2'}>Ajustar</Typography>
          </Paper>
        </div>
      </div>
      <div className={classes.bodyProducts}>
        <SearchField setSearch={setSearch} />

        {loading
          ? (
            <div className={classes.root}>
              <LinearProgress />
            </div>
          )
          : <SearchTable data={result} title={'Productos'} />
        }
      </div>
    </div>
  )
}
