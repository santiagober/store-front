import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide'
import Axios from 'axios'
import Snackbar from './../feedback/snackbar'
import RegisterArticles from './articles/registerArticles'
import userContext from './../../userContext'
// import Axios from 'axios'
// import { green } from '@material-ui/core/colors'
// import SearchIcon from '@material-ui/icons/Search'
import Grid from '@material-ui/core/Grid'
// import TextField from '@material-ui/core/TextField'
// import Checkbox from '@material-ui/core/Checkbox';
// import Tooltip from '@material-ui/core/Tooltip';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Switch from '@material-ui/core/Switch';
// import DeleteIcon from '@material-ui/icons/Delete';
// import FilterListIcon from '@material-ui/icons/FilterList';

// function createData(name, calories, fat, carbs, protein) {
//   return { name, calories, fat, carbs, protein }
// }

// const rows = [
//   createData('Cupcake', 305, 3.7, 67, 4.3),
//   createData('Donut', 452, 25.0, 51, 4.9),
//   createData('Eclair', 262, 16.0, 24, 6.0),
//   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//   createData('Gingerbread', 356, 16.0, 49, 3.9),
//   createData('Honeycomb', 408, 3.2, 87, 6.5),
//   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//   createData('Jelly Bean', 375, 0.0, 94, 0.0),
//   createData('KitKat', 518, 26.0, 65, 7.0),
//   createData('Lollipop', 392, 0.2, 98, 0.0),
//   createData('Marshmallow', 318, 0, 81, 2.0),
//   createData('Nougat', 360, 19.0, 9, 37.0),
//   createData('Oreo', 437, 18.0, 63, 4.0),
// ]

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order
    return a[1] - b[1];
  })
  return stabilizedThis.map((el) => el[0])
}

const headCells = [
  // { id: 'id', numeric: false, disablePadding: true, label: 'id' },
  { id: 'description', numeric: false, disablePadding: true, label: 'Descripción' },
  { id: 'primaryKey', numeric: false, disablePadding: false, label: 'Clave' },
  { id: 'alternativeKey', numeric: false, disablePadding: false, label: 'Clave alterna' },
  { id: 'category', numeric: false, disablePadding: false, label: 'Categoria' },
  { id: 'prices', numeric: true, disablePadding: false, label: 'Price' },
]

function EnhancedTableHead(props) {
  // const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell
          // padding={"checkbox"}
          key={'actions'}
          align={'left'}
        // padding={headCell.disablePadding ? 'none' : 'default'}
        // sortDirection={orderBy === headCell.id ? order : false}
        >
          {/* <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          /> */}
          Acciones
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
}

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { title } = props;

  return (
    <Toolbar
      variant={'dense'}
      className={clsx(classes.root)}
    >
      {/* {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected} selected
        </Typography>
      ) : ( */}
      <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
        {title}
      </Typography>
      {/* )} */}

      {/* {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : ( */}
      {/* <Tooltip title="Filter list">
        <IconButton aria-label="filter list">
          <FilterListIcon />
        </IconButton>
      </Tooltip> */}
      {/* )} */}
    </Toolbar>
  )
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginLeft: theme.spacing(1)
  },
  iconAdd: {
    color: theme.palette.success.dark
  },
  iconDelete: {
    color: theme.palette.error.dark
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function EnhancedTable({ data, title, search }) {
  const classes = useStyles()
  const [rows, setRows] = React.useState(data)
  const [order, setOrder] = React.useState('asc')
  const [orderBy, setOrderBy] = React.useState('description')
  const [selected, setSelected] = React.useState([])
  const [page, setPage] = React.useState(0)
  const [openDial, setOpenDial] = React.useState(false)
  const [openSnack, setOpenSnack] = React.useState(false)
  const [typeSnack, setTypeSnack] = React.useState('')
  const [idProd, setIdProd] = React.useState(null)
  const [msgeSnack, setMsgeSnack] = React.useState(null)
  const [openRegister, setOpenRegister] = React.useState(false)
  const [article, setArticle] = React.useState(null)
  const useUserContext = useContext(userContext)
  // const [dense, setDense] = React.useState(false)
  // const [rowsPerPage, setRowsPerPage] = React.useState(6)
  const rowsPerPage = 6

  // useEffect(() => {
  //   const a = getProducts(search)
  //   console.log(a)
  //   if (a.success && a.products) {
  //     setRows(a.products)
  //   }
  // }, [search])

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.id)
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }

  const handleClickDelete = (id) => {
    setIdProd(id)
    setOpenDial(true)
  }

  const handleClickEdit = (id) => {
    Axios.get(`${process.env.REACT_APP_STORE_BACK}/product/${id}`, {
      params: {
        uucomp: useUserContext.uucomp,
        uuagen: useUserContext.uuagen
      },
    })
      .then(({ data: { data, success, message } }) => {
        if (success) {
          setIdProd(id)
          setArticle(data)
          setOpenRegister(true)
          // setTypeSnack('success')
          // setRows(data.filter(dat => dat.id !== idProd))
        }
        else {
          setTypeSnack('error')
        }
        setMsgeSnack(message)
      })
      .catch((e) => {
        setTypeSnack('error')
        setMsgeSnack(e.message)
      })
      .finally(() => {
        setOpenSnack(true)
        setOpenDial(false)
      })
  }

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      )
    }

    setSelected(newSelected)
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleCloseDial = () => {
    setOpenDial(false);
  };

  const handleDeleteProduct = () => {
    Axios.delete(`${process.env.REACT_APP_STORE_BACK}/product/${idProd}`)
      .then(({ data: { success, message } }) => {
        if (success) {
          setTypeSnack('success')
          setRows(data.filter(dat => dat.id !== idProd))
        }
        else {
          setTypeSnack('error')
        }
        setMsgeSnack(message)
      })
      .catch((e) => {
        setTypeSnack('error')
        setMsgeSnack(e.message)
      })
      .finally(() => {
        setOpenSnack(true)
        setOpenDial(false)
      })
  }

  // const handleChangeRowsPerPage = (event) => {
  //   setRowsPerPage(parseInt(event.target.value, 10))
  //   setPage(0)
  // }

  // const handleChangeDense = (event) => {
  //   setDense(event.target.checked)
  // }

  const isSelected = (id) => selected.indexOf(id) !== -1

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage)

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar title={title} numSelected={selected.length} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'small'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.id);
                  // const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.id)}
                      // role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Grid container spacing={0} alignItems="flex-end">
                          <Grid item>
                            <IconButton onClick={() => handleClickEdit(row.id)} size={'small'} aria-label="edit">
                              <EditIcon fontSize={'small'} color={'action'} className={classes.iconAdd} />
                            </IconButton>
                          </Grid>
                          <Grid item>
                            <IconButton onClick={() => handleClickDelete(row.id)} aria-label="delete" size={'small'}>
                              <DeleteForeverIcon fontSize={'small'} className={classes.iconDelete} />
                            </IconButton>
                          </Grid>
                        </Grid>
                      </TableCell>
                      {/* <TableCell component="th" id={labelId} scope="row" padding="none">
                        {row.id}
                      </TableCell> */}
                      <TableCell align="left">{row.description}</TableCell>
                      <TableCell align="left">{row.primaryKey}</TableCell>
                      <TableCell align="left">{row.alternativeKey}</TableCell>
                      <TableCell align="left">{row.category}</TableCell>
                      <TableCell align="right">{row.prices}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (33) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[6]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
        // onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <Dialog
        open={openDial}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleCloseDial}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{"Estás seguro que deseas eliminar el producto?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Una vez eliminado, no podrá recuperarse.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteProduct} color="primary">
            Ok
          </Button>
          <Button onClick={handleCloseDial} color="secondary">
            Cancelar
          </Button>
        </DialogActions>
      </Dialog>
      {openSnack && msgeSnack && idProd ? <Snackbar setOpenError={setOpenSnack} typeSnack={typeSnack} msge={`${msgeSnack}`} /> : null}
      {openRegister ? <RegisterArticles article={article} setOpenParent={setOpenRegister} /> : null}
      {/* <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Dense padding"
      /> */}
    </div>
  );
}