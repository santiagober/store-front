import React, { useState, useEffect, useReducer } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
// import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios'
import Cookies from 'js-cookie'
import Snackbar from './../feedback/snackbar'
// import ExpandLess from '@material-ui/icons/ExpandLess';
// import ButtonGroup from '@material-ui/core/ButtonGroup';
// import ExpandMore from '@material-ui/icons/ExpandMore';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(6),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  textfield: {
    marginTop: theme.spacing(2)
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    // marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  button: {
    '& > *': {
      margin: theme.spacing(1),
    },
  }
}));

const reducer = (state, action) => {
  switch (action.type) {
    case 'INITIAL_LOAD':
      const company = Cookies.get('uushortcomp')
      const agency = Cookies.get('uushortagen')
      if (company && agency) {
        return {
          ...state,
          companyField: company,
          agencyField: agency,
          moreFields: false
        }
      } else {
        return state
      }
    case 'SET_MOREFIELDS':
      return {
        ...state,
        moreFields: action.moreFields
      }
    case 'SET_STATEBUTTON':
      return {
        ...state,
        stateButton: action.stateButton
      }
    default:
      return state
  }
}

const initialState = {
  agencyField: null,
  companyField: null,
  stateButton: false,
  moreFields: true
}

let company, agency

export default function SignIn({ setVerify, setName }) {
  const [state, dispatch] = useReducer(reducer, initialState)

  const [msgeError, setOpenError] = useState(null)
  // const [agencyField, setAgencyField] = useState(null)
  // const [companyField, setCompanyField] = useState(null)
  // const [stateButton, setStateButton] = useState(false)
  // const [moreFields, setMoreFields] = useState(true)
  const classes = useStyles();

  useEffect(() => {
    dispatch({ type: 'INITIAL_LOAD' })
  }, [])
  // const getMoreFields = () => {
  //   const company = Cookies.get('company')
  //   const agency = Cookies.get('agency')
  //   if (company && agency) {
  //     setAgencyField(agency)
  //     setCompanyField(company)
  //     setMoreFields(false)
  //   }
  // }

  const handleMoreFields = () => {
    // setMoreFields(!moreFields)
    dispatch({ type: 'SET_MOREFIELDS', moreFields: !state.moreFields })
  }

  const onSend = async (e) => {
    // setStateButton(true)
    e.preventDefault()
    dispatch({ type: 'SET_STATEBUTTON', stateButton: true })

    const username = e.target[0].value
    const password = e.target[2].value
    // const companyId = e.target[4].value
    const companyId = company ? company : state.companyField
    // const agencyId = e.target[6].value
    const agencyId = agency ? agency : state.agencyField
    // console.log(companyId + ' - ' + agencyId)
    const { data } = await axios.post(`${process.env.REACT_APP_STORE_BACK}/user/login`, { username, password, companyId, agencyId })
    dispatch({ type: 'SET_STATEBUTTON', stateButton: false })
    if (data.success) {
      Cookies.set('accessToken', data.token, { expires: data.expires })
      Cookies.set('uushortcomp', companyId, { expires: 30 })
      Cookies.set('uushortagen', agencyId, { expires: 30 })
      Cookies.set('uucomp', data.uuagen, { expires: 30 })
      Cookies.set('uuagen', data.uucomp, { expires: 30 })
      setVerify(true)
      setName(username)
    } else {
      setOpenError(data.message)
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      {/* <CssBaseline /> */}
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Inicio de Sesión
        </Typography>
        <form className={classes.form} noValidate onSubmit={onSend}>
          <TextField
            className={classes.textfield}
            variant="outlined"
            margin="none"
            required
            fullWidth
            id="user"
            label="Usuario"
            name="user"
            // autoComplete="User"
            autoFocus
          />
          <TextField
            className={classes.textfield}
            variant="outlined"
            margin="none"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          {state.moreFields &&
            <TextField
              onBlur={(e) => company = e.target.value.toString().trim()}
              defaultValue={state.companyField}
              className={classes.textfield}
              variant="outlined"
              margin="none"
              required
              fullWidth
              id="company"
              label="Compañía"
              name="company"
              autoComplete="company"
            />
          }
          {state.moreFields &&
            <TextField
              onBlur={(e) => agency = e.target.value}
              defaultValue={state.agencyField}
              className={classes.textfield}
              variant="outlined"
              margin="none"
              required
              fullWidth
              id="agency"
              label="Agencia"
              name="agency"
              autoComplete="agency"
            />
          }
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            disabled={state.stateButton}
            className={classes.submit}
          // ref={button}
          >
            Entrar
          </Button>
          <Grid container>
            <Grid item xs>
              <div className={classes.button} >
                <Button onClick={handleMoreFields} color='primary'>{state.moreFields ? 'Menos campos' : 'Mas campos'}</Button>
                {/* <Button>Two</Button>
                <Button>Three</Button> */}
              </div>
              {/* <Link href="#" variant="body2"> */}
              {/* Mas campos {moreFields && <ExpandMore />} */}
              {/* </Link> */}
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={2}>
        <Copyright />
      </Box>
      {msgeError ? <Snackbar setOpenError={setOpenError} typeSnack={'error'} msge={`${msgeError}`} /> : null}
    </Container>
  );
}