import Cookies from 'js-cookie'

const logout = () => {
  Cookies.remove('accessToken')
  Cookies.remove('uuagen')
  Cookies.remove('uucomp')
  window.location.reload()
}

export { logout }