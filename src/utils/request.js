import { useState, useEffect } from 'react'
import Axios from 'axios'

export default function useRequest(type, urlParam, body, headers) {
  const [url, setUrl] = useState(urlParam)
  const [loading, setLoading] = useState(true)
  const [result, setResult] = useState([])

  useEffect(() => {
    const getRequest = async () => {
      try {
        setLoading(true)
        switch (type) {
          case 1:
            const { data: { products, success } } = await Axios.get(url, body)
            if (success)
              setResult(products)
            break
          default:
            console.log(type)
        }
        setLoading(false)
      } catch {
        setLoading(false)
      }
    }
    getRequest()
  }, [body, type, url, headers])

  return [result, loading, setUrl]
}
