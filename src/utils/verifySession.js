import { useState, useEffect } from 'react'
import Cookies from 'js-cookie'
import axios from 'axios'

const useVerifySession = () => {
  const [verify, setVerify] = useState(false)
  const [name, setName] = useState('')
  const [loading, setLoading] = useState(true)
  const [user, setUser] = useState({})

  useEffect(() => {
    try {
      const accessToken = Cookies.get('accessToken')
      const uuagen = Cookies.get('uuagen')
      const uucomp = Cookies.get('uucomp')
      if (accessToken) {
        axios.post(process.env.REACT_APP_STORE_BACK + '/user/protected', { uucomp, uuagen }, {
          headers: {
            Authorization: `Bearer ${accessToken}`
          },
        })
          .then(({ data: { data, success } }) => {
            if (success) {
              setUser(data)
              setName(data.username)
              setVerify(data.session)
            }
            setLoading(false)
          })
          .catch((e) => {
            setLoading(false)
          })
      } else {
        setLoading(false)
      }
    } catch (e) {
      console.log(e)
      setLoading(false)
    }
  }, [])

  return [verify, setVerify, name, setName, loading, user]
}

export { useVerifySession } 